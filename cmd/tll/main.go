package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/sacules/tolistenlist"
)

func main() {
	err := tolistenlist.InitDB(dbPath)
	if err != nil {
		log.Fatal(err)
	}
	defer tolistenlist.CloseDB()

	albums, err := tolistenlist.GetAlbums()
	if err != nil {
		log.Fatal(err)
	}

	tolistenlist.Sort(albums)

	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "status":
			status(albums)

		case "add":
			add()

		case "rand":
			random()

		default:
			fmt.Println("invalid command")
		}

		return
	}

	tui := newTui(albums)
	tui.init()
	tui.setup()
	tui.Run()
}
