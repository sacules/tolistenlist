package main

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/Sacules/input"
	"gitlab.com/sacules/tolistenlist"
)

func status(albums []tolistenlist.Album) {
	var total, listened float32

	for _, al := range albums {
		if al.Score != 0 {
			listened++
		}

		total++
	}

	fmt.Println("Total records:", total)
	fmt.Println("Listened records:", listened)
	fmt.Printf("Percentage listened: %.2f%%\n", listened*100/total)
}

func add() {
	artist := input.Read("Artist: ")
	record := input.Read("Record: ")

	album := tolistenlist.Album{
		Artist: artist,
		Title:  record,
	}

	albums, err := tolistenlist.GetAlbums()
	if err != nil {
		panic(err)
	}

	for _, al := range albums {
		if al.Title == record {
			fmt.Println("record already in db")
			return
		}
	}

	err = album.Save()
	if err != nil {
		panic(err)
	}
}

func random() {
	albums, err := tolistenlist.GetAlbums()
	if err != nil {
		panic(err)
	}

	rand.Seed(time.Now().UnixNano())

	var al tolistenlist.Album
	for {
		n := rand.Intn(len(albums))
		al = albums[n]

		if al.Score == 0 {
			break
		}
	}

	fmt.Println(al.Artist, "-", al.Title)
}
