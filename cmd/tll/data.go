package main

import (
	"path/filepath"

	"github.com/adrg/xdg"
)

const (
	app    = "tll"
	dbName = "records.db"
)

var (
	dbDir  = filepath.Join(xdg.DataHome, app)
	dbPath = filepath.Join(dbDir, dbName)
)
