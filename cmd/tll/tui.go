package main

import (
	"fmt"
	"strconv"

	"github.com/gdamore/tcell"
	"github.com/lithammer/fuzzysearch/fuzzy"
	"github.com/rivo/tview"

	"gitlab.com/sacules/tolistenlist"
)

const (
	columnArtist = iota
	columnRecord
	columnScore
)

type tui struct {
	// TODO: don't compose since these overlap and makes
	// for inconsistent syntax, just rename it properly
	*tview.Application
	*tview.Flex
	*tview.Table

	searchbox  *tview.InputField
	replacebox *tview.InputField
	currentcol int
	currentrow int

	albums []tolistenlist.Album

	// used for searching
	filtered []tolistenlist.Album
}

func newTui(albums []tolistenlist.Album) *tui {
	return &tui{
		Application: tview.NewApplication(),
		Flex:        tview.NewFlex(),
		Table:       tview.NewTable(),
		searchbox:   tview.NewInputField(),
		replacebox:  tview.NewInputField(),
		albums:      albums,
		filtered:    albums,
	}
}

func (tui *tui) tableClean() {
	// Headers
	firstRow := 0

	artist := tview.NewTableCell("Artist")
	album := tview.NewTableCell("Album")
	score := tview.NewTableCell("Score")

	for _, h := range []*tview.TableCell{artist, album, score} {
		h.SetTextColor(tcell.ColorYellow).
			SetAttributes(tcell.AttrBold).
			SetMaxWidth(40).
			SetExpansion(3).
			SetSelectable(false)
	}

	score.SetMaxWidth(5).SetExpansion(1)

	// Main table
	tui.Table.Clear()
	tui.Table.SetCell(firstRow, columnArtist, artist)
	tui.Table.SetCell(firstRow, columnRecord, album)
	tui.Table.SetCell(firstRow, columnScore, score)

	albums, err := tolistenlist.GetAlbums()
	if err != nil {
		panic(err)
	}

	tolistenlist.Sort(albums)
	tui.albums = albums
}

// configure the different widget's visuals
func (tui *tui) init() {
	tview.Borders.HorizontalFocus = tview.BoxDrawingsHeavyHorizontal
	tview.Borders.VerticalFocus = tview.BoxDrawingsHeavyVertical
	tview.Borders.TopLeftFocus = tview.BoxDrawingsHeavyDownAndRight
	tview.Borders.TopRightFocus = tview.BoxDrawingsHeavyDownAndLeft
	tview.Borders.BottomLeftFocus = tview.BoxDrawingsHeavyUpAndRight
	tview.Borders.BottomRightFocus = tview.BoxDrawingsHeavyUpAndLeft

	tui.Table.
		SetSelectable(true, true).
		SetFixed(1, 3).
		SetEvaluateAllRows(true).
		SetBorder(true)

	tui.tableClean()
	tui.tablePrint(tui.albums)

	tui.searchbox.SetFieldBackgroundColor(tcell.ColorBlack)
	tui.replacebox.SetFieldBackgroundColor(tcell.ColorBlack)

	tui.Flex.SetDirection(tview.FlexRow).
		AddItem(tui.Table, 0, 1, true).
		AddItem(tui.searchbox, 1, 0, false)

	tui.
		SetRoot(tui.Flex, true).
		SetFocus(tui.Flex)
}

func (tui *tui) tableInput(event *tcell.EventKey) *tcell.EventKey {
	if event.Rune() == '/' || event.Key() == tcell.KeyCtrlF {
		tui.SetFocus(tui.searchbox)
	}

	if event.Key() == tcell.KeyEnter {
		tui.Flex.RemoveItem(tui.searchbox).
			AddItem(tui.replacebox, 1, 0, false)
		tui.SetFocus(tui.replacebox)

		row, col := tui.Table.GetSelection()
		tui.currentrow = row
		tui.currentcol = col

		cell := tui.Table.GetCell(row, col)
		tui.replacebox.SetText(cell.Text)
	}

	return event
}

// called each time you press Enter on the replacebox
func (tui *tui) replaceDone(key tcell.Key) {
	// Cleanup
	defer func() {
		tui.replacebox.Blur()
		tui.SetFocus(tui.Table)
		tui.Flex.RemoveItem(tui.replacebox).
			AddItem(tui.searchbox, 1, 0, false)
		tui.tableClean()
		tui.tablePrint(tui.filtered)
	}()

	// either we exit or no results are found
	if key == tcell.KeyEsc || len(tui.filtered) == 0 {
		return
	}

	replacetext := tui.replacebox.GetText()
	curralbum := tui.filtered[tui.currentrow-1]

	switch tui.currentcol {
	case columnArtist:
		curralbum.Artist = replacetext

	case columnRecord:
		curralbum.Title = replacetext

	case columnScore:
		if replacetext == "" {
			replacetext = "0"
		}

		score, err := strconv.ParseFloat(replacetext, 64)
		if err != nil {
			tui.searchbox.SetText("couldn't format it: " + err.Error())
			return
		}

		curralbum.Score = score
	}

	tui.filtered[tui.currentrow-1] = curralbum
	err := curralbum.Update()
	if err != nil {
		panic(err)
	}
}

// installs the input capture and other logic functions
func (tui *tui) setup() {
	tui.Table.SetInputCapture(tui.tableInput)
	tui.replacebox.SetDoneFunc(tui.replaceDone)

	// TODO: improve search mechanism for more fine-grained search
	// maybe use a temporary map? for checking if an artist
	// has already been included
	tui.searchbox.SetChangedFunc(func(text string) {
		tui.filtered = make([]tolistenlist.Album, 0)
		for _, al := range tui.albums {
			if fuzzy.MatchFold(text, al.Artist) || fuzzy.MatchFold(text, al.Title) {
				tui.filtered = append(tui.filtered, al)
			}
		}

		//TODO: highlight fuzzy or complete words on results?
		tui.tableClean()
		tui.tablePrint(tui.filtered)
	})

	tui.searchbox.SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEsc {
			tui.tableClean()
			tui.tablePrint(tui.albums)

			tui.searchbox.SetText("")

			// Restore so we don't accidentally use the wrong reference
			tui.filtered = tui.albums
		}

		tui.searchbox.Blur()
		tui.SetFocus(tui.Table)
	})

	tui.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if (event.Rune() == 'q' || event.Key() == tcell.KeyEsc) && (tui.GetFocus() != tui.searchbox && tui.GetFocus() != tui.replacebox) {
			tui.Stop()
		}

		return event
	})
}

func (tui *tui) tablePrint(albums []tolistenlist.Album) {
	// Start at 1 to skip the headers
	row := 1

	for _, al := range albums {
		artname := tview.NewTableCell(al.Artist).SetMaxWidth(50)
		albname := tview.NewTableCell(al.Title).SetMaxWidth(50)

		rating := tview.NewTableCell("").SetMaxWidth(5)
		if al.Score != 0 {
			rating.Text = fmt.Sprintf("%.2f", al.Score)
		}

		tui.Table.SetCell(row, columnArtist, artname)
		tui.Table.SetCell(row, columnRecord, albname)
		tui.Table.SetCell(row, columnScore, rating)

		row++
	}
}
