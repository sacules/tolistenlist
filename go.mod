module gitlab.com/sacules/tolistenlist

go 1.13

require (
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/Sereal/Sereal v0.0.0-20191211210414-3a6c62eca003 // indirect
	github.com/adrg/xdg v0.2.1
	github.com/asdine/storm v2.1.2+incompatible
	github.com/etcd-io/bbolt v1.3.3
	github.com/gdamore/tcell v1.3.0
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.3.1
	github.com/lithammer/fuzzysearch v1.1.0
	github.com/rivo/tview v0.0.0-20200219210816-cd38d7432498
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	gitlab.com/Sacules/input v0.1.1
	gitlab.com/sacules/jsonfile v0.2.2
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
