package tolistenlist

import (
	"fmt"
	"os"
	"sort"
	"strings"
	"testing"

	bolt "github.com/etcd-io/bbolt"
	"github.com/google/go-cmp/cmp"
	"gitlab.com/sacules/jsonfile"
)

func TestUnexportedRate(t *testing.T) {
	var err error

	// Empty case
	emptyArt := new(Artist)
	err = emptyArt.rate("", 0)
	if err == nil {
		t.Error(err)
	}

	err = emptyArt.rate("foo", 0)
	if err == nil {
		t.Error(err)
	}

	// usual case
	normalArt := &Artist{
		"Anathema",
		[]Record{
			{"Weather Systems", 5},
			{"Judgement", 4.5},
			{"We're Here Because We're Here", 4},
			{"A Natural Disaster", 4.5},
			{"Distant Satellites", 4},
		},
	}
	err = normalArt.rate("", 0)
	if err == nil {
		t.Error(err)
	}

	err = normalArt.rate("foo", 0)
	if err == nil {
		t.Error(err)
	}

	err = normalArt.rate("Judgement", 5)
	if err != nil {
		t.Error(err)
	}
}

func TestExportedRate(t *testing.T) {
	var err error
	dbName := "test_rate.db"

	err = InitDB(dbName)
	if err != nil {
		t.Error(err)
	}
	defer CloseDB()
	defer os.Remove(dbName)

	// Empty case
	emptyArt := new(Artist)
	err = emptyArt.Rate("", 0)
	if err == nil {
		t.Error(err)
	}

	err = emptyArt.Rate("foo", 0)
	if err == nil {
		t.Error(err)
	}

	// usual case
	// TODO: Put this in a separate function that generates it!
	normalArt := &Artist{
		"Anathema",
		[]Record{
			{"A Natural Disaster", 4.5},
			{"Weather Systems", 5},
			{"Judgement", 4.5},
		},
	}
	err = normalArt.Rate("", 0)
	if err == nil {
		t.Error(err)
	}

	err = normalArt.Rate("foo", 0)
	if err == nil {
		t.Error(err)
	}

	err = normalArt.Rate("Judgement", 5)
	if err != nil {
		t.Error(err)
	}

	artists, err := GetAll()
	if err != nil {
		t.Error(err)
	}
	for _, art := range artists {
		for _, rec := range art.Records {
			if rec.Name == "Judgement" && rec.Rating != 5 {
				t.Errorf("didn't properly update the rating: expected 5, got %.2f", rec.Rating)
			}
		}
	}
}

func TestGetBucket(t *testing.T) {
	var err error
	dbName := "test_get_bucket.db"

	err = InitDB(dbName)
	if err != nil {
		t.Error(err)
	}
	defer CloseDB()
	defer os.Remove(dbName)

	emptyArt := new(Artist)
	err = db.Update(func(tx *bolt.Tx) error {
		bucket, err := emptyArt.getBucket(tx)
		if bucket != nil {
			return fmt.Errorf("expected nil bucket, got: %v", bucket)
		}

		return err
	})
	if err == nil {
		t.Error(err)
	}

	normalArt := &Artist{Name: "Massive Attack"}
	err = db.Update(func(tx *bolt.Tx) error {
		bucket, err := normalArt.getBucket(tx)
		if bucket == nil {
			return fmt.Errorf("expected nil bucket, got: %v", bucket)
		}

		return err
	})
	if err != nil {
		t.Error(err)
	}
}

func TestAdd(t *testing.T) {
	emptyArt := new(Artist)

	err := emptyArt.add("")
	if err != nil {
		t.Error(err)
	}

	err = emptyArt.add("The Dark Side of the Moon")
	if err != nil {
		t.Error(err)
	}

	err = emptyArt.add("The Dark Side of the Moon")
	if err == nil {
		t.Errorf("expected error: %v", *emptyArt)
	}
}

func TestRemove(t *testing.T) {
	emptyArt := new(Artist)

	err := emptyArt.remove("")
	if err == nil {
		t.Errorf("expected error: %v", *emptyArt)
	}

	err = emptyArt.remove("Lust For Life")
	if err == nil {
		t.Errorf("expected error: %v", *emptyArt)
	}

	normalArt := &Artist{
		"Anathema",
		[]Record{
			{"A Natural Disaster", 4.5},
			{"Weather Systems", 5},
			{},
			{"Judgement", 4.5},
		},
	}

	err = normalArt.remove("")
	if err != nil {
		t.Errorf("expected error: %v", *normalArt)
	}

	err = normalArt.remove("Judgement")
	if err != nil {
		t.Errorf("expected error: %v", *normalArt)
	}

	for _, rec := range normalArt.Records {
		if rec.Name == "Judgement" {
			t.Error("didn't properly remove the record")
		}
	}
}

func TestGetall(t *testing.T) {
	var err error
	dbName := "records.db"

	err = InitDB(dbName)
	if err != nil {
		t.Error(err)
	}
	defer CloseDB()

	dbArtists, err := GetAll()
	if err != nil {
		t.Error(err)
	}

	var jsonArtists []*Artist
	err = jsonfile.Load(&jsonArtists, "records.json")
	if err != nil {
		t.Error(err)
	}

	sort.SliceStable(jsonArtists, func(i, j int) bool {
		return strings.ToLower(jsonArtists[i].Name) < strings.ToLower(jsonArtists[j].Name)
	})

	sort.SliceStable(dbArtists, func(i, j int) bool {
		return strings.ToLower(dbArtists[i].Name) < strings.ToLower(dbArtists[j].Name)
	})

	if !cmp.Equal(jsonArtists, dbArtists) {
		t.Error(cmp.Diff(jsonArtists, dbArtists))
	}
}
