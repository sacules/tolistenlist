package tolistenlist

import (
	"fmt"
	"sort"
	"strings"

	"github.com/asdine/storm"
)

var (
	db *storm.DB
)

// Album represents a recording, with artists' name along with a rating
type Album struct {
	ID int `storm:"increment,unique"`

	Artist string  `json:"artist"`
	Title  string  `json:"title"`
	Score  float64 `json:"score"`
}

// InitDB takes care of initiating the inner database with the given name.
// Returns an error if it couldn't be properly started.
func InitDB(name string) error {
	var err error
	db, err = storm.Open(name)
	if err != nil {
		return fmt.Errorf("init database: %v", err)
	}

	return nil
}

// CloseDB closes the inner database. Returns an error if resources couldn't
// be properly cleaned up
func CloseDB() error {
	return db.Close()
}

// GetAlbums returns a slice of every single record in the db
func GetAlbums() ([]Album, error) {
	var albums []Album
	err := db.All(&albums)
	if err != nil {
		return nil, fmt.Errorf("get albums: %v", err)
	}

	return albums, nil
}

// Save the album's data to the db.
func (al Album) Save() error {
	err := db.Save(&al)
	if err != nil {
		return fmt.Errorf("saving album: %v", err)
	}

	return nil
}

// Update a record
func (al Album) Update() error {
	err := db.Update(&al)
	if err != nil {
		return fmt.Errorf("updating album: %v", err)
	}

	return nil
}

// Sort the albums
func Sort(albums []Album) {
	sort.Slice(albums, func(i, j int) bool {
		art1 := strings.ToLower(albums[i].Artist)
		art2 := strings.ToLower(albums[j].Artist)

		if art1 == art2 {
			return strings.ToLower(albums[i].Title) < strings.ToLower(albums[j].Title)
		}

		return art1 < art2
	})
}
